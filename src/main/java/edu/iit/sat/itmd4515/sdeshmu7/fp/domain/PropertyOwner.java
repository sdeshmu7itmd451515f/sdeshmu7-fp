/*
 * Property Owner entity created to property owner to sell his property online.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * Property Owner publishes the Property For Sale or Rent .He can own more than
 * one Property
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "PropertyOwner.findByName", query = "select powner from PropertyOwner powner where powner.name= :name"),
    @NamedQuery(name = "PropertyOwner.findAll", query = "select powner from PropertyOwner powner"),
    @NamedQuery(name = "PropertyOwner.findByUsername", query = "select powner from PropertyOwner powner where powner.user.userName = :username")
})
public class PropertyOwner extends BaseEntity {

    private String name;
    private String address;
    private String city;
    private String state;
    private long pinCode;
    private long mobileNumber;
    @Temporal(TemporalType.DATE)
    private Date ownsPropertyFromDate;

    /**
     *
     */
    public PropertyOwner() {
    }

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param state
     * @param pinCode
     * @param mobileNumber
     * @param ownsPropertyFromDate
     */
    public PropertyOwner(String name, String address, String city, String state, long pinCode, long mobileNumber, Date ownsPropertyFromDate) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.pinCode = pinCode;
        this.mobileNumber = mobileNumber;
        this.ownsPropertyFromDate = ownsPropertyFromDate;
    }

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param state
     * @param pinCode
     * @param mobileNumber
     * @param ownsPropertyFromDate
     * @param user
     */
    public PropertyOwner(String name, String address, String city, String state, long pinCode, long mobileNumber, Date ownsPropertyFromDate, User user) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.pinCode = pinCode;
        this.mobileNumber = mobileNumber;
        this.ownsPropertyFromDate = ownsPropertyFromDate;
        this.user = user;
    }

    //Mp3 Spec : Mapping to User Table as each PorpertyOwner will have unique ID
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "username")
    private User user;

    //As one Property Owner can own more than one Property we 1:n relation with the Property Entity.
    @OneToMany(mappedBy = "propertyOwner", cascade = CascadeType.ALL)
    private List<Property> property = new ArrayList<>();

    /**
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public List<Property> getProperty() {
        return property;
    }

    /**
     *
     * @param property
     */
    public void setProperty(List<Property> property) {
        this.property = property;
    }

    /**
     * Helper Method to add a new Property to collection ,while managing both
     * side of Relationship
     *
     * @param p
     */
    public void addProperty(Property p) {
        if (!this.property.contains(p)) {
            this.property.add(p);
        }
        p.setPropertyOwner(this);
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    public long getPinCode() {
        return pinCode;
    }

    /**
     *
     * @param pinCode
     */
    public void setPinCode(long pinCode) {
        this.pinCode = pinCode;
    }

    /**
     *
     * @return
     */
    public long getMobileNumber() {
        return mobileNumber;
    }

    /**
     *
     * @param mobileNumber
     */
    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     *
     * @return
     */
    public Date getOwnsPropertyFromDate() {
        return ownsPropertyFromDate;
    }

    /**
     *
     * @param ownsPropertyFromDate
     */
    public void setOwnsPropertyFromDate(Date ownsPropertyFromDate) {
        this.ownsPropertyFromDate = ownsPropertyFromDate;
    }

    @Override
    public String toString() {
        return "PropertyOwner{" + "propertyOwnerId=" + id + ", name=" + name + ", address=" + address + ", city=" + city + ", state=" + state + ", pinCode=" + pinCode + ", mobileNumber=" + mobileNumber + ", ownsPropertyFromDate=" + ownsPropertyFromDate + '}';
    }

}
