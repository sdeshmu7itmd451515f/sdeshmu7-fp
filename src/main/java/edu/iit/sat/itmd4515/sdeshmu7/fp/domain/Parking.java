/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * Parking will have the information about the Parking Space ,Parking cost
 * etc.Each Property can have Zero or one Parking .
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Parking.findBySpace", query = "select p from Parking p where p.parkingSpace= :space "),
    @NamedQuery(name = "Parking.findAll", query = "select p from Parking p ")
})

public class Parking extends BaseEntity {

    private float parkingSpace;
    private float price;
    private boolean parkingAvailable;

    /**
     *
     */
    public Parking() {
    }

    /**
     *
     * @param parkingSpace
     * @param price
     * @param parkingAvailable
     */
    public Parking(float parkingSpace, float price, boolean parkingAvailable) {
        this.parkingSpace = parkingSpace;
        this.price = price;
        this.parkingAvailable = parkingAvailable;
    }

    //1:1 mapping because one Property can have only one parking.
    @OneToOne(mappedBy = "parking")
    private Property property;

    /**
     *
     * @return
     */
    public Property getProperty() {
        return property;
    }

    /**
     *
     * @param property
     */
    public void setProperty(Property property) {
        this.property = property;
    }

    /**
     *
     * @return
     */
    public float getParkingSpace() {
        return parkingSpace;
    }

    /**
     *
     * @param parkingSpace
     */
    public void setParkingSpace(float parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    /**
     *
     * @return
     */
    public float getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     *
     * @return
     */
    public boolean isParkingAvailable() {
        return parkingAvailable;
    }

    /**
     *
     * @param parkingAvailable
     */
    public void setParkingAvailable(boolean parkingAvailable) {
        this.parkingAvailable = parkingAvailable;
    }

    @Override
    public String toString() {
        return "Parking{" + "parkingID=" + id + ", parkingSpace=" + parkingSpace + ", price=" + price + ", parkingAvailable=" + parkingAvailable + '}';
    }

}
