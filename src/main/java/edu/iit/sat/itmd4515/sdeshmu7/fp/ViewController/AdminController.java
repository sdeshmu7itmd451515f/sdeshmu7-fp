/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.ViewController;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Agent;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Buyer;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.PropertyOwner;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.Group;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.AgentService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.BuyerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.GroupService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyOwnerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.web.LoginController;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Sandeep
 */
@Named
@RequestScoped
public class AdminController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(AdminController.class.getName());

    /*Creating List Objects to display all the Agent,PropertyOwners,Buyers and Property.
     Admin has also the authority to create an new Agent,Buyer or Property Owner 
     and Edit the Property details listed by the Property Owner */
    private List<Agent> allAgents;
    private List<Property> allProperty;
    private List<PropertyOwner> allPropertyOwner;
    private List<Buyer> allBuyer;
    private Property propertyForUpdate;

    private Agent newAgentForRems;
    private Buyer newBuyerForRems;
    private PropertyOwner newPropertyOwnerForRems;
    private User userForRems;
    private Group newGroup;

    @EJB
    AgentService agentService;
    @EJB
    PropertyService propertyService;
    @EJB
    BuyerService buyerService;
    @EJB
    GroupService groupservice;
    @EJB
    PropertyOwnerService propertyOwnerService;
    @Inject
    AdminController adminController;
    @Inject
    LoginController loginController;

    public AdminController() {
    }

    @Override
    @PostConstruct
    protected void postConstruct() {
        newAgentForRems = new Agent();
        newBuyerForRems = new Buyer();
        newPropertyOwnerForRems = new PropertyOwner();
        userForRems = new User();

        propertyForUpdate = new Property();
        //   allAgents=new ArrayList<>();
        super.postConstruct(); //To change body of generated methods, choose Tools | Templates.
    }

    public void refreshPropertyList() {

        allProperty = propertyService.findAll();
    }

    public String doUpdateProperty(Property property) {
        LOG.info("Preparing to update" + property.toString());
        this.propertyForUpdate = property;
        return "editProperty";
    }

    public String executeUpdateProperty() {
        LOG.info("Preparing to Update a new Property ");
        facesContext.addMessage(null, new FacesMessage("Property at " + propertyForUpdate.getAddress() + "has been Updated !"));
        propertyService.update(propertyForUpdate);

        refreshPropertyList();

        return "displayAllProperty";
    }

    public String doDeleteProperty(Property property) {
        LOG.info("Preparing to delete" + property.toString());
        facesContext.addMessage(null, new FacesMessage("Property at " + property.getAddress() + "has been Deleted !"));
        //delete the Property
        propertyService.delete(property);
        refreshPropertyList();
        return "displayAllProperty";
    }

    //executeCreateNewAgent
    public String doCreateAgent() {
        LOG.info("Preparing to create a new Agent for REMS ");
        newAgentForRems = new Agent();
        userForRems = new User();
        return "createNewAgent";
    }

    public String executeCreateNewAgent() {
        LOG.info("Preparing to create a new Property ");
        facesContext.addMessage(null, new FacesMessage("A New Agent has been Created !"));

        newGroup = groupservice.findGroupAgent();
        userForRems.addUserToGroup(newGroup);
        agentService.create(newAgentForRems, userForRems);

        return "Welcome";
    }

    public String doCreateBuyer() {
        LOG.info("Preparing to create a new Buyer for REMS ");
        newBuyerForRems = new Buyer();
        userForRems = new User();

        return "createNewBuyer";
    }

    public String executeCreateNewBuyer() {
        LOG.info("Preparing to create a new Buyer ");
        facesContext.addMessage(null, new FacesMessage("A New Buyer has been Created !"));

        newGroup = groupservice.findGroupBuyer();
        userForRems.addUserToGroup(newGroup);
        buyerService.create(newBuyerForRems, userForRems);

        return "Welcome";
    }

    public String doCreatePropertyOwner() {
        LOG.info("Preparing to create a new Property Owner for REMS ");
        newPropertyOwnerForRems = new PropertyOwner();
        userForRems = new User();

        return "createNewPropertyOwner";
    }

    public String executeCreateNewPropertyOwner() {
        LOG.info("Preparing to create a new Property Owner ");
        facesContext.addMessage(null, new FacesMessage("A New Property Owner has been Created !"));

        newGroup = groupservice.findGroupBuyer();
        userForRems.addUserToGroup(newGroup);
        propertyOwnerService.create(newPropertyOwnerForRems, userForRems);

        return "Welcome";
    }

    public List<Agent> getAllAgents() {
        return allAgents;
    }

    public void setAllAgents(List<Agent> allAgents) {
        this.allAgents = allAgents;
    }

    public List<Property> getAllProperty() {
        return allProperty;
    }

    public void setAllProperty(List<Property> allProperty) {
        this.allProperty = allProperty;
    }

    public List<PropertyOwner> getAllPropertyOwner() {
        return allPropertyOwner;
    }

    public void setAllPropertyOwner(List<PropertyOwner> allPropertyOwner) {
        this.allPropertyOwner = allPropertyOwner;
    }

    public List<Buyer> getAllBuyer() {
        return allBuyer;
    }

    public void setAllBuyer(List<Buyer> allBuyer) {
        this.allBuyer = allBuyer;
    }

    public Property getPropertyForUpdate() {
        return propertyForUpdate;
    }

    public void setPropertyForUpdate(Property propertyForUpdate) {
        this.propertyForUpdate = propertyForUpdate;
    }

    public Agent getNewAgentForRems() {
        return newAgentForRems;
    }

    public void setNewAgentForRems(Agent newAgentForRems) {
        this.newAgentForRems = newAgentForRems;
    }

    public Buyer getNewBuyerForRems() {
        return newBuyerForRems;
    }

    public void setNewBuyerForRems(Buyer newBuyerForRems) {
        this.newBuyerForRems = newBuyerForRems;
    }

    public PropertyOwner getNewPropertyOwnerForRems() {
        return newPropertyOwnerForRems;
    }

    public void setNewPropertyOwnerForRems(PropertyOwner newPropertyOwnerForRems) {
        this.newPropertyOwnerForRems = newPropertyOwnerForRems;
    }

    public User getUserForRems() {
        return userForRems;
    }

    public void setUserForRems(User userForRems) {
        this.userForRems = userForRems;
    }

    public Group getNewGroup() {
        return newGroup;
    }

    public void setNewGroup(Group newGroup) {
        this.newGroup = newGroup;
    }

}
