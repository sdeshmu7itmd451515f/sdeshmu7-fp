/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.ViewController;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Agent;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.AgentService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.web.LoginController;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Sandeep
 */
@Named
@RequestScoped
public class AgentController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(AgentController.class.getName());

    private List<Agent> agents;
    private List<Property> property;
    private Property propertyForAgent;
    private Agent agent;

    @EJB
    AgentService agentService;
    @EJB
    PropertyService propertyService;
    @Inject
    LoginController loginController;

    public AgentController() {
    }

    @Override
    @PostConstruct
    protected void postConstruct() {
        propertyForAgent = new Property();
        agent = agentService.findByUsername(loginController.getRemoteUser());
        property = agent.getProperty();
        super.postConstruct(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Display the selected Property in more Detail
     *
     * @param property to display
     * @return the page on which Property will be displayed to the User with
     * more Detail
     */
    public String doShowProperty(Property property) {
        LOG.info("Preparing to Show Property " + property.toString());
        this.propertyForAgent = property;

        return "displayProperty";
    }

    /**
     * Create a new Property to be managed by the Agent
     *
     * @return
     */
    public String doCreateProperty() {
        LOG.info("Preparing to create a new Property ");
        propertyForAgent = new Property();

        return "newProperty";
    }

    public void refreshPropertyList() {

        property = agentService.findByUsername(loginController.getRemoteUser()).getProperty();
    }

    /**
     * Handle the action from the newProperty view
     *
     * @return
     */
    public String executeCreateProperty() {
        LOG.info("Preparing to create a new Property ");
        facesContext.addMessage(null, new FacesMessage("Property at " + propertyForAgent.getAddress() + "has been Created !"));
        //Create Property
//          List<Agent> agentList = new ArrayList<>();
//        agentList.add(agent);
//         
//        propertyForAgent.setAgents(agentList);
//        
//        propertyService.create(propertyForAgent);

        propertyService.create(propertyForAgent, agent);

        refreshPropertyList();

        return "Welcome";
    }

    /**
     * Handle the action from the Edit property view
     *
     * @return
     */
    public String executeUpdateProperty() {
        LOG.info("Preparing to Update a new Property ");
        facesContext.addMessage(null, new FacesMessage("Property at " + propertyForAgent.getAddress() + "has been Updated !"));
        propertyService.update(propertyForAgent, agent);

        refreshPropertyList();

        return "Welcome";
    }

    /**
     *
     * @param Prepare property to be updated
     * @return the page on which the Property will be displayed for update
     */
    public String doUpdateProperty(Property property) {
        LOG.info("Preparing to update" + property.toString());
        this.propertyForAgent = property;
        return "editProperty";
    }

    /**
     *
     * @param Delete the property by the Agent
     * @return sends the user back to the Welcome page with message
     */
    public String doDeleteProperty(Property property) {
        LOG.info("Preparing to delete" + property.toString());
        facesContext.addMessage(null, new FacesMessage("Property at " + property.getAddress() + "has been Deleted !"));
        //delete the Property
        propertyService.delete(property, agent);
        refreshPropertyList();
        return "Welcome";
    }

    /**
     * Get the value of agents
     *
     * @return the value of agents
     */
    public List<Agent> getAgents() {
        return agents;
    }

    /**
     * Set the value of agents
     *
     * @param agents new value of agents
     */
    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }

    /**
     * Get the value of property
     *
     * @return the value of property
     */
    public List<Property> getProperty() {
        return property;
    }

    /**
     * Set the value of property
     *
     * @param property new value of property
     */
    public void setProperty(List<Property> property) {
        this.property = property;
    }

    public Property getPropertyForAgent() {
        return propertyForAgent;
    }

    public void setPropertyForAgent(Property propertyForAgent) {
        this.propertyForAgent = propertyForAgent;
    }

}
