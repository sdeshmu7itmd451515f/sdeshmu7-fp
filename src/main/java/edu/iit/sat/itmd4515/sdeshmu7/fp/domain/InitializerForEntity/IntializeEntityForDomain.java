/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain.InitializerForEntity;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Agent;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Buyer;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Parking;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Payment;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.PropertyOwner;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.Group;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.AgentService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.BuyerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.GroupService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.ParkingService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PaymentService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyOwnerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.UserService;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * MP3 Spec: Created Singleton class and used Startup annotation for
 * implementing the CRUD functionality
 */
@Singleton
@Startup
public class IntializeEntityForDomain {

    @EJB
    PropertyService propertyService;

    @EJB
    PaymentService paymentService;

    @EJB
    AgentService agentService;

    @EJB
    BuyerService buyerService;

    @EJB
    PropertyOwnerService propertyOwnerService;

    @EJB
    ParkingService parkingService;

    @EJB
    UserService userService;

    @EJB
    GroupService groupService;

    /**
     * default Constructor
     */
    public IntializeEntityForDomain() {
    }

    @PostConstruct
    private void doInit() {

        //Adding groups to have combine the access for different users
        Group agents = new Group("agents", "This group is for Agents only");
        Group buyers = new Group("buyers", "This group is Buyers only");
        Group propertyOwners = new Group("propertyOwners", "This Group is for Property Owners only");
        Group admins = new Group("dba", "This is the Database administrator group who is responsible for maintainence of the Website");

        groupService.create(agents);
        groupService.create(buyers);
        groupService.create(propertyOwners);
        groupService.create(admins);

        User agent1 = new User("Agent1", "one");
        User agent2 = new User("Agent2", "two");
        User agent3 = new User("Agent3", "three");

        User buyer1 = new User("Buyer1", "one");
        User buyer2 = new User("Buyer2", "two");
        User buyer3 = new User("Buyer3", "three");

        User propertyOwner1 = new User("Propertyowner1", "one");
        User propertyOwner2 = new User("Propertyowner2", "two");
        User propertyOwner3 = new User("Propertyowner3", "three");

        User adminForSystem1 = new User("SysAdmin", "password");
        User adminForSystem2 = new User("DbAdmin", "password");

        agent1.addUserToGroup(agents);
        agent2.addUserToGroup(agents);
        agent3.addUserToGroup(agents);

        buyer1.addUserToGroup(buyers);
        buyer2.addUserToGroup(buyers);
        buyer3.addUserToGroup(buyers);

        propertyOwner1.addUserToGroup(propertyOwners);
        propertyOwner2.addUserToGroup(propertyOwners);
        propertyOwner3.addUserToGroup(propertyOwners);

        adminForSystem1.addUserToGroup(admins);
        adminForSystem2.addUserToGroup(admins);

        userService.create(agent1);
        userService.create(agent2);
        userService.create(agent3);

        userService.create(buyer1);
        userService.create(buyer2);
        userService.create(buyer3);

        userService.create(propertyOwner1);
        userService.create(propertyOwner2);
        userService.create(propertyOwner3);

        userService.create(adminForSystem1);
        userService.create(adminForSystem2);

        //PROPERTY :Records added
        Property property1 = new Property("Jackson", "Chicago", "Illinois", 60005, 20000, "Rent", "Yes", new GregorianCalendar(2015, 10, 06).getTime());
        Property property2 = new Property("Van buren Avenue", "Chicago", "Illinois", 60001, 20000, "Rent", "NO", new GregorianCalendar(2015, 10, 06).getTime());
        Property property3 = new Property("West Adams Avenue", "Chicago", "Illinois", 60003, 20000, "Sale", "NO", new GregorianCalendar(2015, 10, 06).getTime());
        Property property4 = new Property("Wabash Avenue", "Chicago", "Illinois", 60000, 20000, "Sale", "Yes", new GregorianCalendar(2015, 10, 06).getTime());

        //PARKING :Records added
        Parking parkingRecord1 = new Parking(900, 100, true);
        property1.setParking(parkingRecord1);
        Parking parkingRecord2 = new Parking(1500, 100, false);
        property2.setParking(parkingRecord2);
        Parking parkingRecord3 = new Parking(2000, 100, true);
        property3.setParking(parkingRecord3);
        Parking parkingRecord4 = new Parking(1000, 90, true);

        //Payment :Records added
        Payment paymentRecord1 = new Payment("Cash", 100450, new GregorianCalendar(2015, 10, 01).getTime());
        Payment paymentRecord2 = new Payment("Card", 45098, new GregorianCalendar(2015, 06, 21).getTime());
        Payment paymentRecord3 = new Payment("Cash", 78900, new GregorianCalendar(2015, 04, 31).getTime());
        Payment paymentRecord4 = new Payment("Card", 78990, new GregorianCalendar(2013, 12, 06).getTime());

        //BUYERS : Records added
        Buyer buyerRecord1 = new Buyer("Sandy", "3041,S.Michigan", "Chicago", "Male", 60616, new GregorianCalendar(1989, 12, 06).getTime(), buyer1);
        Buyer buyerRecord2 = new Buyer("Guru", "3011,S.Michigan", "Chicago", "Male", 60616, new GregorianCalendar(1989, 12, 06).getTime(), buyer2);
        Buyer buyerRecord3 = new Buyer("Dhruv", "prarie shores", "Chicago", "Male", 60616, new GregorianCalendar(2015, 12, 06).getTime(), buyer3);
        Buyer buyerRecord4 = new Buyer("Akshay", "lake meadows", "Chicago", "Male", 60616, new GregorianCalendar(2014, 10, 06).getTime());

        //Property Owner: Records added
        PropertyOwner pownerRecord1 = new PropertyOwner("Raymond", "Willow county", "Chicago", "Illinois", 60000, 2222222, new GregorianCalendar(2009, 12, 06).getTime(), propertyOwner1);
        PropertyOwner pownerRecord2 = new PropertyOwner("Nixon Camelion", "Orange county", "Chicago", "Illinois", 60000, 2222222, new GregorianCalendar(2012, 12, 06).getTime(), propertyOwner2);
        PropertyOwner pownerRecord3 = new PropertyOwner("Dafnie Reynolds", "Cook county", "Chicago", "Illinois", 60000, 2222222, new GregorianCalendar(2014, 12, 06).getTime(), propertyOwner3);
        PropertyOwner pownerRecord4 = new PropertyOwner("Shiela Lawson", "Will county", "Chicago", "Illinois", 60000, 2222222, new GregorianCalendar(2013, 12, 06).getTime());

        //Agents :Records added
        Agent agentRecord1 = new Agent("Scott", "staurt building", "Chicago", "Illinois", 60616, 312713434, agent1);
        Agent agentRecord2 = new Agent("Sandeep", "Engineering building", "Schamburg", "Illinois", 60616, 312713434, agent2);
        Agent agentRecord3 = new Agent("Ankit", "Life Science building", "Iowa", "Illinois", 60616, 312713434, agent3);

        //Agent1 "Ankit" manages property 
        List<Property> propertyList = new ArrayList<>();
        propertyList.add(property3);
        agentRecord1.setProperty(propertyList);

        //Agent2 "Sandeep" manages two Property
        List<Property> propertyListForAgent2 = new ArrayList<>();
        propertyListForAgent2.add(property1);
        propertyListForAgent2.add(property2);
        agentRecord2.setProperty(propertyListForAgent2);

        // Property Owner "Raymond" can own multiple properties
        List<Property> propertyListOfOwners = new ArrayList<>();
        propertyListOfOwners.add(property1);
        propertyListOfOwners.add(property2);
        pownerRecord1.setProperty(propertyListOfOwners);

        //Buyer1 "Sandy" Buys Property 
        List<Property> propertyListOfBuyers = new ArrayList<>();
        propertyListOfBuyers.add(property1);
        buyerRecord1.setProperty(propertyListOfBuyers);

        //Property will have mutiple agents managing the Property
        List<Agent> agentList = new ArrayList<>();
        agentList.add(agentRecord1);
        agentList.add(agentRecord2);

        //Property To Agents has N:N relationship .So One property can have more than one agents to manage the property.
        property1.setAgents(agentList);

        //Property Relation With Buyers
        buyerRecord1.addProperty(property1);
        buyerRecord1.addProperty(property4);

        property1.setPropertyOwner(pownerRecord1);
        property2.setPropertyOwner(pownerRecord2);
        property3.setPropertyOwner(pownerRecord1);

        //Creating the Records for all the entities in the class
        //Property Create
        propertyService.create(property1);
        propertyService.create(property2);
        propertyService.create(property3);
        propertyService.create(property4);

        //Agents Create
        agentService.create(agentRecord1);
        agentService.create(agentRecord2);
        agentService.create(agentRecord3);

        //Payment Records persist
        paymentService.create(paymentRecord1);
        paymentService.create(paymentRecord2);
        paymentService.create(paymentRecord3);
        paymentService.create(paymentRecord4);

        //Parking Record Persists 
        parkingService.create(parkingRecord1);
        parkingService.create(parkingRecord2);
        parkingService.create(parkingRecord3);
        parkingService.create(parkingRecord4);

        //Buyers Records Persist
        buyerService.create(buyerRecord1);
        buyerService.create(buyerRecord2);
        buyerService.create(buyerRecord3);
        buyerService.create(buyerRecord4);

        //Property Owner Persists
        propertyOwnerService.create(pownerRecord1);
        propertyOwnerService.create(pownerRecord2);
        propertyOwnerService.create(pownerRecord3);
        propertyOwnerService.create(pownerRecord4);

        /* *******************Update Operation On Property Table ***********************/
        property4.setAddress("Milvaukee");
        propertyService.update(property4);

        doRead();
        doUpdate();
        doDelete();
    }

    private void doRead() {

        //Read Records for Property Table
        System.out.println("=====================================Property Read All Records Start========================================\n");
        List<Property> propertyForRetrieve = propertyService.findAll();
        for (Property propertyResult : propertyForRetrieve) {
            System.out.println("Record:" + propertyResult.toString());
        }
        System.out.println("=====================================Property Read All Records End========================================\n");

        //Read Records for Parking Table
        System.out.println("=====================================Parking Read All Records Start========================================\n");
        List<Parking> parking = parkingService.findAll();
        for (Parking parkingResult : parking) {
            System.out.println("Record:" + parkingResult.toString());
        }
        System.out.println("=====================================Parking Read All Records End========================================\n");

        //Read All for Payment Table
        System.out.println("=====================================Payment Read All Records Start========================================\n");
        List<Payment> payment1 = paymentService.findAll();
        for (Payment paymentResult : payment1) {
            System.out.println("Record:" + paymentResult.toString());
        }
        System.out.println("=====================================Payment Read All Records End========================================\n");

        //Read All for Buyer table
        System.out.println("=====================================Buyer Read All Records Start========================================\n");
        List<Buyer> buyer = buyerService.findAll();
        for (Buyer buyerResult : buyer) {
            System.out.println("Record:" + buyerResult.toString());
        }
        System.out.println("=====================================Buyer Read All Records End========================================\n");

        //Read All for Property Owner
        System.out.println("=====================================Property Owner Read All Records Start========================================\n");
        List<PropertyOwner> powner1 = propertyOwnerService.findAll();
        for (PropertyOwner pOwnerResult : powner1) {
            System.out.println("Record:" + pOwnerResult.toString());
        }
        System.out.println("=====================================Property Owner Read All Records End========================================\n");

        //Read All for Agents
        System.out.println("=====================================Agents Read All Records Start========================================\n");
        List<Agent> agent = agentService.findAll();
        for (Agent agentResult : agent) {
            System.out.println("Record:" + agentResult.toString());
        }
        System.out.println("=====================================Agents Read All Records End========================================\n");

    }

    private void doUpdate() {

        //Updating the Property Table for CRUD demonstration
        System.out.println("=====================================Property Table :Creating a Record using Persist Start========================================\n");
        Property propertyUpdate = new Property("Indiana Dunes", "SpringField", "Illinois", 60000, 20000, "Sale", "NO", new GregorianCalendar(2015, 10, 06).getTime());
        propertyService.update(propertyUpdate);
        System.out.println("=====================================Property Table :Creating a Record using Persist Start========================================\n");
    }

    private void doDelete() {
        //Agent Table Delete operation starts here
        System.out.println("=====================================Agent Table :Delete a Record Start========================================\n");
        List<Agent> agent = agentService.findAll();

        for (Agent agentResult : agent) {
            if (agentResult.getName().equalsIgnoreCase("George")) {
                agentService.delete(agentResult);
                System.out.println("Record Deleted is ==>" + agentResult.getName() + "\nRecord :" + agentResult.toString());
            }
        }
        System.out.println("=====================================Agent Table :Delete a Record End========================================\n");

        //Sample Delete for Parking
        System.out.println("=====================================Parking Table :Delete a Record Start========================================\n");
        Parking parkingForDeletion = parkingService.find(3L);
        System.out.println("Record Deleted is ==>" + parkingForDeletion.getId() + "\nRecord :" + parkingForDeletion.toString());
        parkingService.delete(parkingForDeletion);
        System.out.println("=====================================Parking Table :Delete a Record End========================================\n");

    }
}
