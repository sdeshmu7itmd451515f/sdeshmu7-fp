/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.web;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Agent;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Buyer;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.PropertyOwner;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.AgentService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.BuyerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyOwnerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * Login Module implemented using Servlet
 */
@WebServlet(name = "RealEstateLoginServlet", urlPatterns = {"/RealEstateLoginServlet"})
public class RealEstateLoginServlet extends HttpServlet {

    @EJB
    PropertyService psrvc;

    @EJB
    AgentService asrvc;

    @EJB
    BuyerService bsrvc;

    @EJB
    PropertyOwnerService posrvc;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>REMS Login</title>");
            out.println("</head>");
            out.println("<body bgcolor=\"#AAAAFF\">");
            out.println("<h1>Welcome to the Real Estate Management System </h1>");

            System.out.println("Remote User" + request.getRemoteUser());
            if (request.isUserInRole("SystemAgents")) {
                Agent a = asrvc.findByUsername(request.getRemoteUser());
                out.println("<h1>Welcome Agent: " + a.getName() + " <br> Agent User Name:" + a.getUserAsAgent().getUserName() + "<br> Agent Mapped To Role : SystemAgents");
                out.println("<ul>");
                for (Property prop : a.getProperty()) {
                    out.println("<li> Property ID: " + prop.getId() + " Property Details: " + prop.toString() + "</li>");
                }
                out.println("</ul>");

            } else if (request.isUserInRole("SystemBuyers")) {
                Buyer buyer = bsrvc.findByUsername(request.getRemoteUser());
                out.println("<h1>Welcome Buyer : " + buyer.getName() + "<br> Buyer User Name: " + buyer.getUser().getUserName() + "<br> Buyer Mapped To Role : SystemBuyers");

                out.println("<ul>");
                for (Property prop : buyer.getProperty()) {
                    out.println("<li> Property ID: " + prop.getId() + "<br> Property Details: " + prop.toString() + "</li>");
                }
                out.println("</ul>");

            } else if (request.isUserInRole("SystemPropertyOwners")) {
                PropertyOwner propertyOwner = posrvc.findByUsername(request.getRemoteUser());
                out.println("<h1>Welcome Property Owner : " + propertyOwner.getName() + "<br> Porperty Owner User Name: " + propertyOwner.getUser().getUserName() + "<br> Property Owner Mapped To Role : SystemPropertyOwners");

                out.println("<ul>");
                for (Property prop : propertyOwner.getProperty()) {
                    out.println("<li> Property ID: " + prop.getId() + " <br>Property Details: " + prop.toString() + "</li>");
                }
                out.println("</ul>");

            } else if (request.isUserInRole("SystemAdmins")) {

                out.println("<h1>Welcome System Administrator: " + request.getRemoteUser());

            } else {
                out.println("<h1>You are not in any roles.  Please Enter Valid Credentials !!!!</h1>");
            }

            out.println("<br><br><a href=\"" + request.getContextPath() + "/logout\">Logout</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
