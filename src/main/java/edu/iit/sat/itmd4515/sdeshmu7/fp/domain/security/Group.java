/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * Used to Handle Security . User are mapped to the groups.
 */
@Entity
@Table(name = "sec_group")
@NamedQueries({
    @NamedQuery(name = "Group.findAll", query = "select g from Group g"),
    @NamedQuery(name = "Group.findAgentGroup", query = "select g from Group g where g.groupName='Agents'"),
    @NamedQuery(name = "Group.findBuyerGroup", query = "select g from Group g where g.groupName='buyers'"),
    @NamedQuery(name = "Group.findProjectOwnerGroup", query = "select g from Group g where g.groupName='propertyOwners'")
})
public class Group {

    @Id
    private String groupName;
    private String groupDescription;

    //As user can be a part of more than 1 group we have n:n relation 
    @ManyToMany(mappedBy = "userGroups")
    private List<User> groupMembers = new ArrayList<>();

    /**
     *
     */
    public Group() {
    }

    /**
     *
     * @param groupName
     * @param groupDescription
     */
    public Group(String groupName, String groupDescription) {
        this.groupName = groupName;
        this.groupDescription = groupDescription;
    }

    /**
     * Get the value of groupDescription
     *
     * @return the value of groupDescription
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * Set the value of groupDescription
     *
     * @param groupDescription new value of groupDescription
     */
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    /**
     * Get the value of groupName
     *
     * @return the value of groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Set the value of groupName
     *
     * @param groupName new value of groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     *
     * @return
     */
    public List<User> getGroupMembers() {
        return groupMembers;
    }

    /**
     *
     * @param groupMembers
     */
    public void setGroupMembers(List<User> groupMembers) {
        this.groupMembers = groupMembers;
    }

}
