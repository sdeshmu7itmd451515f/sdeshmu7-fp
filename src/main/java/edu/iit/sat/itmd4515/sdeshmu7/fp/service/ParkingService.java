/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Parking;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for Parking
 */
@Stateless
public class ParkingService extends AbstractService<Parking> {

    /**
     *
     */
    public ParkingService() {
        super(Parking.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Parking> findAll() {
        return em.createNamedQuery("Parking.findAll", Parking.class).getResultList();
    }
}
