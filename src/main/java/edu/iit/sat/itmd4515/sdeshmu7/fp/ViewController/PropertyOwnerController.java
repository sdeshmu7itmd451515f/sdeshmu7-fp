/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.ViewController;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.PropertyOwner;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyOwnerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.web.LoginController;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Sandeep
 */
@Named
@RequestScoped
public class PropertyOwnerController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(PropertyOwnerController.class.getName());

    private List<Property> allProperty;
    private Property propertyForOwner;
    private PropertyOwner propertyOwner;

    @EJB
    PropertyService propertyService;
    @EJB
    PropertyOwnerService propertyOwnerService;
    @Inject
    LoginController loginController;

    public PropertyOwnerController() {
    }

    @Override
    @PostConstruct
    protected void postConstruct() {

        propertyForOwner = new Property();
        propertyOwner = propertyOwnerService.findByUsername(loginController.getRemoteUser());
        allProperty = propertyOwner.getProperty();
        super.postConstruct();
    }

    public void refreshPropertyList() {
        allProperty = propertyOwnerService.findByUsername(loginController.getRemoteUser()).getProperty();
    }

    /**
     * Create a new Property to be Owned by Property OWner
     *
     * @return
     */
    public String doCreateProperty() {
        LOG.info("Preparing to create a new Property ");
        propertyForOwner = new Property();
        return "newProperty";
    }

    public String executeCreateProperty() {
        LOG.info("Preparing to create a new Property ");
        facesContext.addMessage(null, new FacesMessage("Property at " + propertyForOwner.getAddress() + "has been Created !"));
        //Create Property

        propertyService.create(propertyForOwner, propertyOwner);

        refreshPropertyList();

        return "Welcome";
    }

    public String doShowProperty(Property property) {
        LOG.info("Preparing to Show Property " + property.toString());
        this.propertyForOwner = property;
        return "displayProperty";
    }

    public String doUpdateProperty(Property property) {
        LOG.info("Preparing to update" + property.toString());
        this.propertyForOwner = property;
        return "editProperty";
    }

    public String executeUpdateProperty() {
        LOG.info("Preparing to Update a new Property ");
        facesContext.addMessage(null, new FacesMessage("Property at " + propertyForOwner.getAddress() + "has been Updated !"));
        propertyService.update(propertyForOwner, propertyOwner);

        refreshPropertyList();

        return "Welcome";
    }

    public String doDeleteProperty(Property property) {
        LOG.info("Preparing to delete" + property.toString());
        facesContext.addMessage(null, new FacesMessage("Property at " + property.getAddress() + "has been Deleted !"));
        //delete the Property
        propertyService.delete(property, propertyOwner);
        refreshPropertyList();
        return "Welcome";
    }

    public List<Property> getAllProperty() {
        return allProperty;
    }

    public void setAllProperty(List<Property> allProperty) {
        this.allProperty = allProperty;
    }

    public Property getPropertyForOwner() {
        return propertyForOwner;
    }

    public void setPropertyForOwner(Property propertyForOwner) {
        this.propertyForOwner = propertyForOwner;
    }

    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }

    public void setPropertyOwner(PropertyOwner propertyOwner) {
        this.propertyOwner = propertyOwner;
    }

}
