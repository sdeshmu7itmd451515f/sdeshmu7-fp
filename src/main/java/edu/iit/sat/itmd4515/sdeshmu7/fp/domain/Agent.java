/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * Agent help the Sale of Real Estate and manage the Property for Customer
 * Presentation
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Agent.findByName", query = "select agent from Agent agent where agent.name= :name"),
    @NamedQuery(name = "Agent.findAll", query = "select agent from Agent agent"),
    @NamedQuery(name = "Agent.findByUsername", query = "select a from Agent a where a.userAsAgent.userName = :username")
})
public class Agent extends BaseEntity implements Comparable<Agent> {

    private String name;
    private String address;
    private String city;
    private String state;
    private long pinCode;
    private long mobileNumber;

    /**
     *
     */
    public Agent() {
    }

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param state
     * @param pinCode
     * @param mobileNumber
     */
    public Agent(String name, String address, String city, String state, long pinCode, long mobileNumber) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.pinCode = pinCode;
        this.mobileNumber = mobileNumber;
    }

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param state
     * @param pinCode
     * @param mobileNumber
     * @param userAsAgent
     */
    public Agent(String name, String address, String city, String state, long pinCode, long mobileNumber, User userAsAgent) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.pinCode = pinCode;
        this.mobileNumber = mobileNumber;
        this.userAsAgent = userAsAgent;
    }

    //Mp3 Spec : One to One mapping with User 
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "username")
    private User userAsAgent;

    /**
     *
     * @return
     */
    public User getUserAsAgent() {
        return userAsAgent;
    }

    /**
     *
     * @param userAsAgent
     */
    public void setUserAsAgent(User userAsAgent) {
        this.userAsAgent = userAsAgent;
    }

    // Many Agents can manage many properties.
    @ManyToMany(mappedBy = "agents", cascade = CascadeType.PERSIST)
    private List<Property> property = new ArrayList<>();

    /**
     *
     * @return
     */
    public List<Property> getProperty() {
        return property;
    }

    /**
     *
     * @param property
     */
    public void setProperty(List<Property> property) {
        this.property = property;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    public long getPinCode() {
        return pinCode;
    }

    /**
     *
     * @param pinCode
     */
    public void setPinCode(long pinCode) {
        this.pinCode = pinCode;
    }

    /**
     *
     * @return
     */
    public long getMobileNumber() {
        return mobileNumber;
    }

    /**
     *
     * @param mobileNumber
     */
    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return "Agent{" + "agentId=" + id + ", name=" + name + ", address=" + address + ", city=" + city + ", state=" + state + ", pinCode=" + pinCode + ", mobileNumber=" + mobileNumber + '}';
    }

    @Override
    public int compareTo(Agent o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
