/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Buyer;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.TypedQuery;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for Buyer
 */
@Named
@Stateless
public class BuyerService extends AbstractService<Buyer> {

    /**
     *
     */
    public BuyerService() {
        super(Buyer.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Buyer> findAll() {
        return em.createNamedQuery("Buyer.findAll", Buyer.class).getResultList();
    }

    /**
     *
     * @param username
     * @return
     */
    public Buyer findByUsername(String username) {
        TypedQuery<Buyer> query = em.createNamedQuery("Buyer.findByUsername", Buyer.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    /**
     * 
     * @param newProperty
     * @param buyer 
     */
    public void update(Property newProperty, Buyer buyer) {

        Property currentProperty = em.getReference(Property.class, newProperty.getId());
        System.out.println("Property id" + currentProperty.toString());
        Buyer updateBuyer = currentProperty.getBuyerRelation();
        currentProperty.setPropertySoldOrRented("Yes");
        buyer.addProperty(currentProperty);

    }

    /**
     * 
     * @param newBuyer
     * @param newUser 
     */
    public void create(Buyer newBuyer, User newUser) {

        newBuyer.setUser(newUser);

        em.persist(newBuyer);
    }
}
