/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Payment;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for Payment
 */
@Named
@Stateless
public class PaymentService extends AbstractService<Payment> {

    /**
     *
     */
    public PaymentService() {
        super(Payment.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Payment> findAll() {
        return em.createNamedQuery("Payment.findAll", Payment.class).getResultList();
    }

}
