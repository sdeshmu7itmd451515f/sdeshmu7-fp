/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.Group;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for Group
 */
@Stateless
public class GroupService extends AbstractService<Group> {

    /**
     *
     */
    public GroupService() {
        super(Group.class);
    }

    @Override
    public List<Group> findAll() {
        return em.createNamedQuery("Group.findAll", Group.class).getResultList();
    }

    public Group findGroupAgent() {
        return em.createNamedQuery("Group.findAgentGroup", Group.class).getSingleResult();
    }

    public Group findGroupBuyer() {
        return em.createNamedQuery("Group.findBuyerGroup", Group.class).getSingleResult();
    }

    public Group findGroupPropertyOwner() {
        return em.createNamedQuery("Group.findProjectOwnerGroup", Group.class).getSingleResult();
    }
}
