/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * Property Entity used to store the details about the Property .
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Property.findByAddress", query = "select prop from Property prop where prop.address= :addr"),
    @NamedQuery(name = "Property.findByZip", query = "select proper from Property proper where proper.zip= :ziparg"),
    @NamedQuery(name = "Property.findAll", query = "select prop from Property prop "),
    @NamedQuery(name = "Property.findAllAvailableProperty", query = "select prop from Property prop where prop.propertySoldOrRented='No'")
})
public class Property extends BaseEntity {

    private String address;
    private String city;
    private String state;
    private long zip;
    private float price;
    private String propertySoldOrRented;
    private String rentOrSale;
    @Temporal(TemporalType.DATE)
    private Date dateAvailableFrom;

    /**
     *
     */
    public Property() {
    }

    //As one owner can have more than 1 Property Setting the realtion as n:1
    @ManyToOne
    private PropertyOwner propertyOwner;

    // One buyer can buy more than one Property so setting up the relation as n:1
    @ManyToOne
    private Buyer buyerRelation;

    //As more than one Agent can handle Same Property and Agents can handle multiple Property we use n:n realtion.
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "AgentsManage_Property",
            joinColumns = @JoinColumn(name = "prop_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "agent_id", referencedColumnName = "id"))
    private List<Agent> agents = new ArrayList<>();

    /**
     *
     * @param address
     * @param city
     * @param state
     * @param zip
     * @param price
     * @param rentOrSale
     * @param dateAvailableFrom
     */
    public Property(String address, String city, String state, long zip, float price, String rentOrSale, String propertyRentorSold, Date dateAvailableFrom) {
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.price = price;
        this.rentOrSale = rentOrSale;
        this.propertySoldOrRented = propertyRentorSold;
        this.dateAvailableFrom = dateAvailableFrom;
    }

    //Helper Method to add an Agent to the List.
    public void addAgent(Agent a) {
        if (!this.agents.contains(a)) {
            this.agents.add(a);
        }
        if (!a.getProperty().contains(this)) {
            a.getProperty().add(this);
        }
    }

    /**
     *
     * @return
     */
    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }

    /**
     *
     * @param propertyOwner
     */
    public void setPropertyOwner(PropertyOwner propertyOwner) {
        this.propertyOwner = propertyOwner;
    }

    /**
     *
     * @return
     */
    public Buyer getBuyerRelation() {
        return buyerRelation;
    }

    /**
     *
     * @param buyerRelation
     */
    public void setBuyerRelation(Buyer buyerRelation) {
        this.buyerRelation = buyerRelation;
    }

    /**
     *
     * @return
     */
    public List<Agent> getAgents() {
        return agents;
    }

    /**
     *
     * @param agents
     */
    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }

    @OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "id")
    private Parking parking;

    /**
     *
     * @return
     */
    public Parking getParking() {
        return parking;
    }

    /**
     *
     * @param parking
     */
    public void setParking(Parking parking) {
        this.parking = parking;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    public long getZip() {
        return zip;
    }

    /**
     *
     * @param zip
     */
    public void setZip(long zip) {
        this.zip = zip;
    }

    /**
     *
     * @return
     */
    public float getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     *
     * @return
     */
    public String getRentOrSale() {
        return rentOrSale;
    }

    /**
     *
     * @param rentOrSale
     */
    public void setRentOrSale(String rentOrSale) {
        this.rentOrSale = rentOrSale;
    }

    /**
     *
     * @return
     */
    public Date getDateAvailableFrom() {
        return dateAvailableFrom;
    }

    /**
     *
     * @param dateAvailableFrom
     */
    public void setDateAvailableFrom(Date dateAvailableFrom) {
        this.dateAvailableFrom = dateAvailableFrom;
    }

    public String getPropertySoldOrRented() {
        return propertySoldOrRented;
    }

    public void setPropertySoldOrRented(String propertySoldOrRented) {
        this.propertySoldOrRented = propertySoldOrRented;
    }

    @Override
    public String toString() {
        return "Property{" + "address=" + address + ", city=" + city + ", state=" + state + ", zip=" + zip + ", price=" + price + ", PropertyLeasedOrSold=" + propertySoldOrRented + ", rentOrSale=" + rentOrSale + ", dateAvailableFrom=" + dateAvailableFrom + ", propertyOwner=" + propertyOwner + ", buyerRelation=" + buyerRelation + ", agents=" + agents + ", parking=" + parking + '}';
    }

}
