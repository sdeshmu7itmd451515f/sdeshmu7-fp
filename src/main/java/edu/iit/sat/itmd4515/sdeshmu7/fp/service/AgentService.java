/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Agent;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.TypedQuery;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for Agent
 */
@Named
@Stateless
public class AgentService extends AbstractService<Agent> {

    /**
     *
     */
    public AgentService() {
        super(Agent.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Agent> findAll() {
        return em.createNamedQuery("Agent.findAll").getResultList();
    }

    /**
     *
     * @param username
     * @return
     */
    public Agent findByUsername(String username) {
        TypedQuery<Agent> query = em.createNamedQuery("Agent.findByUsername", Agent.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    public void create(Agent newAgent, User newUser) {

        newAgent.setUserAsAgent(newUser);

        em.persist(newAgent);
    }
}
