/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.PropertyOwner;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.TypedQuery;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for PropertyOwner
 */
@Named
@Stateless
public class PropertyOwnerService extends AbstractService<PropertyOwner> {

    /**
     *
     */
    public PropertyOwnerService() {
        super(PropertyOwner.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<PropertyOwner> findAll() {
        return em.createNamedQuery("PropertyOwner.findAll", PropertyOwner.class).getResultList();
    }

    /**
     *
     * @param username
     * @return
     */
    public PropertyOwner findByUsername(String username) {
        TypedQuery<PropertyOwner> query = em.createNamedQuery("PropertyOwner.findByUsername", PropertyOwner.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    public void create(PropertyOwner newPropertyOwner, User newUser) {

        newPropertyOwner.setUser(newUser);

        em.persist(newPropertyOwner);
    }
}
