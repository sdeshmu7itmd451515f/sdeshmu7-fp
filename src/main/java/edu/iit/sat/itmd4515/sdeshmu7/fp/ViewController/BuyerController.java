/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.ViewController;

import edu.iit.sat.itmd4515.sdeshmu7.fp.cdi.EmailServiceBean;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Buyer;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Payment;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.BuyerService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PaymentService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.service.PropertyService;
import edu.iit.sat.itmd4515.sdeshmu7.fp.web.LoginController;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Sandeep
 */
@Named
@RequestScoped
public class BuyerController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(BuyerController.class.getName());

    private List<Property> allProperty;
    private List<Property> allPropertyForBuyer;

    private Property buyerProperty;

    private Buyer buyer;

    private Payment paymentForProperty;

    private Property buyerPaymentProperty;

    @EJB
    PropertyService propertyService;
    @EJB
    BuyerService buyerService;
    @EJB
    PaymentService paymentService;
    @Inject
    LoginController loginController;
    @Inject
    EmailServiceBean emailServiceBean;

    public BuyerController() {
    }

    @Override
    @PostConstruct
    protected void postConstruct() {
        buyerPaymentProperty = new Property();
        buyerProperty = new Property();
        paymentForProperty = new Payment();
        buyer = buyerService.findByUsername(loginController.getRemoteUser());
        allPropertyForBuyer = buyer.getProperty();
        super.postConstruct();
    }

    public String doShowProperty(Property property) {
        LOG.info("Preparing to Show Property" + property.toString());
        this.buyerProperty = property;
        return "displayProperty";
    }

    public String doBuyProperty(Property property) {
        LOG.info("Preparing to Buy Property" + property.toString());
        this.buyerProperty = property;
        return "buyProperty";
    }

    public String executeBuyProperty() {
        LOG.info("Preparing to Update a new Property ");
        facesContext.addMessage(null, new FacesMessage("Property at " + buyerProperty.getAddress() + "has been Updated !"));
        buyerService.update(buyerProperty, buyer);
         emailServiceBean.sendMail("sandep.deshmukh@gmail.com", "Property Transaction", "The Property has been Purchased/Rented by you Successfully");
        paymentForProperty = new Payment();

        return "paymentOfBuyer";
    }

    public String executeUpdatePayment() {
        LOG.info("Preparing to Update make a payment of Property ");
        facesContext.addMessage(null, new FacesMessage("Payment for has been done Successfully!"));
        System.out.println("In the Execute Payment Method");
        paymentService.update(paymentForProperty);
        emailServiceBean.sendMail("sandep.deshmukh@gmail.com", "Property Transaction", "The Property has been Purchased/Rented by you Successfully");
        refreshPropertyList();
        refreshAllPropertyList();

        return "Welcome";
    }

    public void refreshAllPropertyList() {

        allProperty = propertyService.findAll();
    }

    public void refreshPropertyList() {
        allPropertyForBuyer = buyerService.findByUsername(loginController.getRemoteUser()).getProperty();
    }

    public List<Property> getAllProperty() {
        return allProperty;
    }

    public void setAllProperty(List<Property> allProperty) {
        this.allProperty = allProperty;
    }

    public List<Property> getAllPropertyForBuyer() {
        return allPropertyForBuyer;
    }

    public void setAllPropertyForBuyer(List<Property> allPropertyForBuyer) {
        this.allPropertyForBuyer = allPropertyForBuyer;
    }

    public Property getBuyerProperty() {
        return buyerProperty;
    }

    public void setBuyerProperty(Property buyerProperty) {
        this.buyerProperty = buyerProperty;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    public Payment getPaymentForProperty() {
        return paymentForProperty;
    }

    public void setPaymentForProperty(Payment paymentForProperty) {
        this.paymentForProperty = paymentForProperty;
    }

    public Property getBuyerPaymentProperty() {
        return buyerPaymentProperty;
    }

    public void setBuyerPaymentProperty(Property buyerPaymentProperty) {
        this.buyerPaymentProperty = buyerPaymentProperty;
    }

}
