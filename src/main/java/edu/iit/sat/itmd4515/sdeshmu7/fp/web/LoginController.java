/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.web;

import edu.iit.sat.itmd4515.sdeshmu7.fp.ViewController.AbstractJSFController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sandeep
 */
@Named
@RequestScoped
public class LoginController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    @NotNull(message = "You must enter a User Name")
    private String username;
    @NotNull(message = "You must enter a password")
    private String password;

    public LoginController() {
    }

    @Override
    @PostConstruct
    protected void postConstruct() {

        super.postConstruct();
    }

    public String getRemoteUser() {
        return facesContext.getExternalContext().getRemoteUser();
    }

    public boolean isAdmin() {
        return facesContext.getExternalContext().isUserInRole("SystemAdmins");
    }

    public boolean isAgent() {
        return facesContext.getExternalContext().isUserInRole("SystemAgents");
    }

    public boolean isPropertyOwner() {
        return facesContext.getExternalContext().isUserInRole("SystemPropertyOwners");
    }

    public boolean isBuyer() {
        return facesContext.getExternalContext().isUserInRole("SystemBuyers");
    }

    public String getRoleContextPath(String path) {
        if (isAdmin()) {
            return "/admin/" + path + FACES_REDIRECT;
        } else if (isAgent()) {
            return "/agentPortal/" + path + FACES_REDIRECT;
        } else if (isPropertyOwner()) {
            return "/propertyOwnerPortal/" + path + FACES_REDIRECT;
        } else if (isBuyer()) {
            return "/buyerPortal/" + path + FACES_REDIRECT;
        }

        return path;
    }

    public String dologin() {
        HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();

        try {
            req.login(username, password);
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, "Failed login from " + username, ex);
            facesContext.addMessage(null, new FacesMessage("Bad Login", "You have entered a bad username or password.Contact the Administrator for SignUp!"));
            return "login.xhtml";
        }

        return getRoleContextPath("Welcome.xhtml");
    }

    public String doLogout() {
        HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();

        try {
            req.logout();
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, "Failed login from " + username, ex);
            facesContext.addMessage(null, new FacesMessage("Bad Logout"));
            return "login.xhtml";
        }
        return "/login.xhtml";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the value of password
     *
     * @return the value of password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the value of password
     *
     * @param password new value of password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
