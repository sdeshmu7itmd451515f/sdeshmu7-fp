/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * Payment Entity is used to make payments towards the Buyer purchase
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Payment.findByAmount", query = "select pmnt from Payment pmnt where pmnt.amount= :amount"),
    @NamedQuery(name = "Payment.findAll", query = "select pmnt from Payment pmnt")
})
public class Payment extends BaseEntity {

    private String typeOfPayment;
    private Long PropertyId;
    private String addressOfProperty;
    private float amount;
    @Temporal(TemporalType.DATE)
    private Date dateOfPayment;

    /**
     *
     */
    public Payment() {
    }

    /**
     *
     * @param typeOfPayment
     * @param amount
     * @param dateOfPayment
     */
    public Payment(String typeOfPayment, float amount, Date dateOfPayment) {
        this.typeOfPayment = typeOfPayment;
        this.amount = amount;
        this.dateOfPayment = dateOfPayment;
    }

    /**
     *
     * @return
     */
    public String getTypeOfPayment() {
        return typeOfPayment;
    }

    /**
     *
     * @param typeOfPayment
     */
    public void setTypeOfPayment(String typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
    }

    /**
     *
     * @return
     */
    public float getAmount() {
        return amount;
    }

    /**
     *
     * @param amount
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     *
     * @return
     */
    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    /**
     *
     * @param dateOfPayment
     */
    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public String getAddressOfProperty() {
        return addressOfProperty;
    }

    public void setAddressOfProperty(String addressOfProperty) {
        this.addressOfProperty = addressOfProperty;
    }

    public Long getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(Long PropertyId) {
        this.PropertyId = PropertyId;
    }

    @Override
    public String toString() {
        return "Payment{" + "typeOfPayment=" + typeOfPayment + ", PropertyId=" + PropertyId + ", addressOfProperty=" + addressOfProperty + ", amount=" + amount + ", dateOfPayment=" + dateOfPayment + '}';
    }

}
