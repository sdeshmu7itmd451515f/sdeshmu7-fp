/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * User Class for storing the Login credentials with hashing of password.
 */
@Entity
@Table(name = "sec_user")
@NamedQuery(name = "User.findAll", query = "select u from User u")
public class User {

    @Id
    private String userName;
    private String password;

    //as One user can belong to multiple groups we use n:n relation
    @ManyToMany
    @JoinTable(name = "sec_user_groups",
            joinColumns = @JoinColumn(name = "username"),
            inverseJoinColumns = @JoinColumn(name = "groupname"))
    List<Group> userGroups = new ArrayList<>();

    /**
     *
     * helper method to add user to the Group table
     *
     * @param g
     */
    public void addUserToGroup(Group g) {
        if (!this.userGroups.contains(g)) {
            this.userGroups.add(g);
        }
        if (!g.getGroupMembers().contains(this)) {
            g.getGroupMembers().add(this);
        }
    }

    /**
     *
     */
    public User() {
    }

    /**
     *
     * @param userName
     * @param password
     */
    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    //Used the default encryption algorithm for Glassfish server i.e SHA 256 
    @PrePersist
    @PreUpdate
    private void hashPassword() {
        String digestPassword = DigestUtils.sha256Hex(this.password);
        this.password = digestPassword;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @return
     */
    public List<Group> getUserGroups() {
        return userGroups;
    }

    /**
     *
     * @param userGroups
     */
    public void setUserGroups(List<Group> userGroups) {
        this.userGroups = userGroups;
    }

}
