/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.domain;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.security.User;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * Buyer will be the Customer Who will make Purchase of Property Or Rent the
 * Property.
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Buyer.findByName", query = "select buyer1 from Buyer buyer1 where buyer1.name= :name"),
    @NamedQuery(name = "Buyer.findAll", query = "select buyer1 from Buyer buyer1"),
    @NamedQuery(name = "Buyer.findByUsername", query = "select buyer1 from Buyer buyer1 where buyer1.user.userName = :username")
})
public class Buyer extends BaseEntity {

    private String name;
    private String address;
    private String city;
    private String gender;
    private long zipCode;
    @Temporal(TemporalType.DATE)
    private Date dateOfApplication;

    /**
     *
     */
    public Buyer() {
    }

    //Mp3 Spec : Mapping to User Table as each Buyer will have unique ID
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "username")
    private User user;

    // One buyer can buy more than one Property So establishing the Relationship 1:n
    @OneToMany(mappedBy = "buyerRelation", cascade = CascadeType.PERSIST)
    private List<Property> property = new ArrayList<>();

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param gender
     * @param zipCode
     * @param dateOfApplication
     */
    public Buyer(String name, String address, String city, String gender, long zipCode, Date dateOfApplication) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.gender = gender;
        this.zipCode = zipCode;
        this.dateOfApplication = dateOfApplication;
    }

    /**
     *
     * @param name
     * @param address
     * @param city
     * @param gender
     * @param zipCode
     * @param dateOfApplication
     * @param user
     */
    public Buyer(String name, String address, String city, String gender, long zipCode, Date dateOfApplication, User user) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.gender = gender;
        this.zipCode = zipCode;
        this.dateOfApplication = dateOfApplication;
        this.user = user;
    }

    /**
     *
     * @return
     */
    public List<Property> getProperty() {
        return property;
    }

    /**
     *
     * @param property
     */
    public void setProperty(List<Property> property) {
        this.property = property;
    }

    /**
     * Helper Method to add a new Property to collection ,while managing both
     * side of Relationship
     *
     * @param p
     */
    public void addProperty(Property p) {
        if (!this.property.contains(p)) {
            this.property.add(p);
        }
        p.setBuyerRelation(this);
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     */
    public long getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     */
    public void setZipCode(long zipCode) {
        this.zipCode = zipCode;
    }

    /**
     *
     * @return
     */
    public Date getDateOfApplication() {
        return dateOfApplication;
    }

    /**
     *
     * @param dateOfApplication
     */
    public void setDateOfApplication(Date dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Buyer{" + "registerId=" + id + ", name=" + name + ", address=" + address + ", city=" + city + ", gender=" + gender + ", zipCode=" + zipCode + ", dob=" + dateOfApplication + '}';
    }

}
