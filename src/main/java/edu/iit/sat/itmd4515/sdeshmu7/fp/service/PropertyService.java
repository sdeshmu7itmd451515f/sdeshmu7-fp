/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.sdeshmu7.fp.service;

import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Agent;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Buyer;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.Property;
import edu.iit.sat.itmd4515.sdeshmu7.fp.domain.PropertyOwner;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * Mp3 Spec: Stateless bean added to implement the Service layer and handle
 * transaction for Property
 */
@Named
@Stateless
public class PropertyService extends AbstractService<Property> {

    /**
     *
     */
    public PropertyService() {
        super(Property.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Property> findAll() {
        return em.createNamedQuery("Property.findAll", Property.class).getResultList();
    }

    public List<Property> findAllAvailableProperty() {
        return em.createNamedQuery("Property.findAllAvailableProperty", Property.class).getResultList();
    }

    public void create(Property property, Agent agent) {
        agent = em.getReference(Agent.class, agent.getId());

        property.addAgent(agent);

        em.persist(property);
    }

    public void create(Property property, PropertyOwner propertyOwner) {
        propertyOwner = em.getReference(PropertyOwner.class, propertyOwner.getId());
        propertyOwner.addProperty(property);
        em.persist(property);
    }

    public void update(Property newProperty, Agent newAgent) {

        Property currentProperty = em.getReference(Property.class, newProperty.getId());
        System.out.println("Property id" + currentProperty.toString());
        List<Agent> currentAgent = currentProperty.getAgents();
        currentProperty.setAddress(newProperty.getAddress());
        currentProperty.setCity(newProperty.getCity());
        currentProperty.setId(newProperty.getId());
        currentProperty.setPrice(newProperty.getPrice());
        currentProperty.setRentOrSale(newProperty.getRentOrSale());

    }

    public void update(Property newProperty, PropertyOwner newAgent) {

        Property currentProperty = em.getReference(Property.class, newProperty.getId());
        System.out.println("Property id" + currentProperty.toString());
        PropertyOwner po = currentProperty.getPropertyOwner();
        currentProperty.setAddress(newProperty.getAddress());
        currentProperty.setCity(newProperty.getCity());
        currentProperty.setId(newProperty.getId());
        currentProperty.setPrice(newProperty.getPrice());
        currentProperty.setRentOrSale(newProperty.getRentOrSale());
        currentProperty.setDateAvailableFrom(newProperty.getDateAvailableFrom());

    }

    /**
     * Delete an existing Property and remove an Agent reference from the
     * Property
     *
     * @param property
     * @param agent
     */
    public void delete(Property property, Agent agent) {
        property = em.getReference(Property.class, property.getId());
        agent = em.getReference(Agent.class, agent.getId());

        agent.getProperty().remove(property);
        property.setAgents(null);

        em.remove(property);
    }

    public void delete(Property property, PropertyOwner propertyOwner) {
        property = em.getReference(Property.class, property.getId());
        propertyOwner = em.getReference(PropertyOwner.class, propertyOwner.getId());

        propertyOwner.getProperty().remove(property);
        property.setPropertyOwner(null);
        em.remove(property);
    }

}
